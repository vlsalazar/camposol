//
//  AppDelegate.swift
//  Camposol
//
//  Created by Victor Salazar on 24/08/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
import UIKit
import CoreData
import Foundation

let baseUrlKey = "com.dsb.camposol.baseUrl"

@UIApplicationMain class AppDelegate:UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var idleTimer:dispatch_cancelable_closure?
    class func sharedAppDelegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    func resetIdleTimer(){
        cancel_delay(closure: idleTimer)
        idleTimer = delay(time: Double(Manager.defaultManager.timeInactivity)){self.idleTimerExceeded()}
    }
    func idleTimerExceeded(){
        if let presentedViewCont = self.window!.rootViewController!.presentedViewController {
            Toolbox.showAlertWithTitle(title: "Alerta", withMessage: "Expiro su sesión", withOkHandler: {(alertAction) -> Void in
                presentedViewCont.dismiss(animated: true, completion: nil)
            }, inViewCont: presentedViewCont)
        }
    }
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        UserDefaults.standard.setValue("1.0.1", forKey: "version")
        UserDefaults.standard.synchronize()
        let baseUrl = UserDefaults.standard.object(forKey: baseUrlKey)
        if baseUrl == nil {
            //http://easyweb.camposol.com.pe
            UserDefaults.standard.set("http://54.225.92.148", forKey: baseUrlKey)
            UserDefaults.standard.synchronize()
        }
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
        UIApplication.shared.applicationIconBadgeNumber = 0
        UIApplication.shared.cancelAllLocalNotifications()
        return true
    }
    func applicationWillResignActive(_ application:UIApplication){
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    func applicationDidEnterBackground(_ application:UIApplication){
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    func applicationWillEnterForeground(_ application:UIApplication){
        UIApplication.shared.applicationIconBadgeNumber = 0
        UIApplication.shared.cancelAllLocalNotifications()
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application:UIApplication){
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    func applicationWillTerminate(_ application:UIApplication){
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    //MARK: - CoreData
    lazy var applicationDocumentsDirectory:NSURL = {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count - 1] as NSURL
    }()
    lazy var managedObjectModel:NSManagedObjectModel = {
        let modelURL = Bundle.main.url(forResource: "Camposol", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    lazy var persistentStoreCoordinator:NSPersistentStoreCoordinator? = {
        var coordinator:NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("Camposol.sqlite")
        var error:NSError? = nil
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator!.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch var error1 as NSError {
            error = error1
            coordinator = nil
            // Report any error we got.
            var dict = [String:AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject
            dict[NSUnderlyingErrorKey] = error
            error = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            abort()
        } catch {
            fatalError()
        }
        return coordinator
    }()
    lazy var managedObjectContext:NSManagedObjectContext? = {
        let coordinator = self.persistentStoreCoordinator
        if coordinator == nil {
            return nil
        }
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    //MARK: - Core Data Saving support
    func saveContext(){
        if let moc = self.managedObjectContext {
            var error:NSError? = nil
            if moc.hasChanges {
                do {
                    try moc.save()
                } catch let error1 as NSError {
                    error = error1
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    NSLog("Unresolved error \(error1), \(error!.userInfo)")
                    abort()
                }
            }
        }
    }
    //MARK: - RemoteNotification
    func application(_ application:UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken:Data){
        let strDeviceToken = deviceToken.map(){String(format: "%02.2hhx", $0)}.joined()
        Manager.defaultManager.deviceId = strDeviceToken
    }
    func application(_ application:UIApplication, didFailToRegisterForRemoteNotificationsWithError error:Error){
        print("\(#function) error: \(error)")
    }
    func application(_ application:UIApplication, didRegister notificationSettings:UIUserNotificationSettings){
        if notificationSettings.types == .none {
            Toolbox.showAlertWithTitle(title: "Alerta", withMessage: "No se pudo obtener el codigo del dispositivo. Dirigase a Ajustes para configurarlo", inViewCont: self.window!.rootViewController!)
        }
    }
}
