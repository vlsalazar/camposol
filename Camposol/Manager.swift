//
//  Manager.swift
//  Camposol
//
//  Created by Victor Salazar on 19/11/15.
//  Copyright © 2015 Victor Salazar. All rights reserved.
//
import Foundation
class Manager{
    static let defaultManager = Manager()
    var userId:Int = -1
    var timeInactivity:Int = 0
    var deviceId:String = "" {
        didSet{
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "managerDeviceId"), object: nil)
        }
    }
}
