//
//  AprobProyTipoFiltro1TableViewCell.swift
//  Camposol
//
//  Created by Victor Salazar on 3/09/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
import UIKit
class ProjApprFromFilterTxtFldTableViewCell:UITableViewCell{
    @IBOutlet weak var nameLbl:UILabel!
    @IBOutlet weak var valueTxtFld:UITextField!
}