//
//  PrincipalViewController.swift
//  Camposol
//
//  Created by Victor Salazar on 25/08/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
import UIKit
class MenuViewController:UITableViewController{
    let options = [Option(title: "Aprobaciones", imageName: "Aprobacion", subpotions: [Option(title: "Formularios", imageName: "Formulario", subpotions: nil)])]
    //let options = [Option(title: "Aprobaciones", imageName: "Aprobacion", subpotions: [Option(title: "Formularios", imageName: "Formulario", subpotions: nil), Option(title: "Proyectos", imageName: "Proyecto", subpotions: nil)])]
    var indexSelected = -1
    var indexStringSelected = ""
    override func viewDidLoad(){
        super.viewDidLoad()
        tableView.layoutMargins = UIEdgeInsets.zero
        AppDelegate.sharedAppDelegate().resetIdleTimer()
    }
    //MARK: - Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if indexStringSelected == "0-0" {
            
        }
    }
    //MARK: - TableView
    override func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        var initialCount = options.count;
        if indexSelected != -1 {
            let optionSelected = options[indexSelected]
            if let suboptions = optionSelected.suboptions {
                initialCount += suboptions.count
            }
        }
        return initialCount
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "optionCell", for: indexPath) as! OptionTableViewCell
        cell.layoutMargins = UIEdgeInsets.zero
        var index = indexPath.row
        var currentOption:Option?
        if indexSelected != -1 {
            if index > indexSelected {
                let optionSelect = options[indexSelected]
                if let suboptions = optionSelect.suboptions {
                    let subindex = index - indexSelected - 1
                    if subindex < suboptions.count {
                        currentOption = suboptions[subindex]
                        index = -1
                    }else{
                        index -= suboptions.count
                    }
                }
            }
        }
        if index != -1 {
            currentOption = options[index]
        }
        if !currentOption!.imageName.isEmpty {
            cell.iconImgView.image = UIImage(named: currentOption!.imageName)
        }
        cell.optionLabel.text = currentOption!.title
        cell.arrowImgView.isHidden = (currentOption?.suboptions == nil)
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let index = indexPath.row
        let prevIndexSelected = indexSelected
        if prevIndexSelected != -1 {
            if index == prevIndexSelected {
                indexSelected = -1
            }else{
                if index < prevIndexSelected {
                    indexSelected = index
                }else{
                    let prevOptionSelected = options[prevIndexSelected]
                    if let suboptions = prevOptionSelected.suboptions {
                        let subindex = index - indexSelected - 1
                        if subindex < suboptions.count {
                            indexStringSelected = "\(indexSelected)-\(subindex)"
                            performSegueWithIndexString()
                            return
                        }else{
                            indexSelected = index - suboptions.count
                        }
                    }else{
                        indexSelected = index
                    }
                }
            }
        }else{
            indexSelected = indexPath.row
        }
        tableView.beginUpdates()
        if prevIndexSelected != -1 {
            let cell = tableView.cellForRow(at: IndexPath(row: prevIndexSelected, section: 0) as IndexPath) as! OptionTableViewCell
            UIView.animate(withDuration: 0.25, animations: {
                cell.arrowImgView.transform = CGAffineTransform.identity
            })
            let prevOptionSelected = options[prevIndexSelected]
            if let suboptions = prevOptionSelected.suboptions {
                var indexPaths = [IndexPath]()
                for i in 1 ... suboptions.count {
                    indexPaths.append(IndexPath(row: (prevIndexSelected + i), section: 0))
                }
                tableView.deleteRows(at: indexPaths, with: .top)
            }
        }
        if indexSelected != -1 {
            let cell = tableView.cellForRow(at: IndexPath(row: indexPath.row, section: 0)) as! OptionTableViewCell
            UIView.animate(withDuration: 0.25, animations: {
                cell.arrowImgView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi) / 2.0)
            })
            let optionSelected = options[indexSelected]
            if let suboptions = optionSelected.suboptions {
                var indexPaths = [IndexPath]()
                for i in 1 ... suboptions.count {
                    indexPaths.append(IndexPath(row: (indexSelected + i), section: 0))
                }
                tableView.insertRows(at: indexPaths, with: .top)
            }
        }
        tableView.endUpdates()
    }
    //MARK: - Auxiliar
    func performSegueWithIndexString(){
        if indexStringSelected == "0-0" {
            performSegue(withIdentifier: "showForms", sender:self)
        }else if indexStringSelected == "0-1" {
            performSegue(withIdentifier: "showApprovProjects", sender:self)
        }
    }
    //MARK: - IBAction
    @IBAction func logout(_: AnyObject){
        SVProgressHUD.show(withStatus: "Cerrando Sesion")
        let dicParams = ["id_usuario": Manager.defaultManager.userId, "id_dispositivo": Manager.defaultManager.deviceId] as [String : Any]
        ServiceConnector.connectToUrl(url: URLs.urlLogout, params: dicParams as AnyObject){(result:AnyObject?, error:Error?) -> Void in
            if error == nil {
                if let dicResult = result as? Dictionary<String, Int> {
                    if let state = dicResult["est"] {
                        if state == 1 {
                            self.dismiss(animated: true, completion: nil)
                            return
                        }else{
                            Toolbox.showAlertWithTitle(title: "Alerta", withMessage: "Hubo un error al intentar cerrar sesión", inViewCont: self)
                        }
                    }else{
                        Toolbox.showAlertWithTitle(title: "Alerta", withMessage: "Hubo un error al intentar cerrar sesión", inViewCont: self)
                    }
                }else{
                    Toolbox.showErrorConnectionInViewCont(viewCont: self)
                }
            }else{
                Toolbox.showErrorConnectionInViewCont(viewCont: self)
            }
        }
    }
}
