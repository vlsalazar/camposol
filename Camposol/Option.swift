//
//  Option.swift
//  Camposol
//
//  Created by Victor Salazar on 25/08/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
import Foundation
class Option{
    var title:String!
    var imageName:String!
    var suboptions:[Option]?
    init(title:String, imageName:String, subpotions:[Option]?){
        self.title = title
        self.imageName = imageName
        self.suboptions = subpotions
    }
}