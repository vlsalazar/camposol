//
//  PannableTableViewCell.swift
//  Camposol
//
//  Created by victor salazar on 7/09/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
import UIKit
class PannableTableViewCell:UITableViewCell{
    @IBOutlet weak var fullLengthView:UIView!
    @IBOutlet weak var buttonsView:UIView!
    var originalPoint = CGPoint.zero
    override func didMoveToSuperview() {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(PannableTableViewCell.handlePan(gesture:)))
        panGesture.delegate = self;
        fullLengthView.addGestureRecognizer(panGesture)
    }
    @objc func handlePan(gesture:UIPanGestureRecognizer){
        let maxTranslateX = -1 * self.buttonsView.frame.width
        if gesture.state == .began {
            originalPoint = gesture.location(in: superview!)
            if fullLengthView.transform.tx == 0.0 {
                buttonsView.transform = CGAffineTransform(translationX: buttonsView.frame.size.width, y: 0)
            }
        }else if gesture.state == .changed {
            let newPoint = gesture.location(in: superview!)
            var translationX = newPoint.x - originalPoint.x
            if translationX + fullLengthView.transform.tx > 0{
                translationX = 0
            }
            fullLengthView.transform = fullLengthView.transform.translatedBy(x: translationX, y: 0)
            buttonsView.transform = buttonsView.transform.translatedBy(x: translationX, y: 0)
            originalPoint = newPoint
        }else if gesture.state == .ended {
            let translateX = fullLengthView.transform.tx
            UIView.animate(withDuration: 0.25, animations: {
                if translateX < maxTranslateX/2.0 {
                    self.fullLengthView.transform = CGAffineTransform(translationX: maxTranslateX, y: 0)
                    self.buttonsView.transform = CGAffineTransform.identity
                }else{
                    self.fullLengthView.transform = CGAffineTransform.identity
                    self.buttonsView.transform = CGAffineTransform(translationX: self.buttonsView.frame.size.width, y: 0)
                }
                }, completion: { (result) -> Void in
                    self.buttonsView.transform = CGAffineTransform.identity
            })
        }
    }
    override func gestureRecognizerShouldBegin(_ gestureRecognizer:UIGestureRecognizer) -> Bool {
        if let panGesture = gestureRecognizer as? UIPanGestureRecognizer {
            let translate = panGesture.translation(in: superview!)
            if fabs(translate.x) > fabs(translate.y) {
                return true
            }
        }
        return false
    }
}
