//
//  CustomTextField.swift
//  Camposol
//
//  Created by Victor Salazar on 3/09/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
import UIKit
class CustomTextField:UITextField{
    override func textRect(forBounds bounds:CGRect) -> CGRect {
        return bounds.insetBy(dx: 6, dy: 0)
    }
    override func editingRect(forBounds bounds:CGRect) -> CGRect {
        return bounds.insetBy(dx: 6, dy: 0)
    }
}
