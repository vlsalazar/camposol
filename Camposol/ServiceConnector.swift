//
//  ServiceConnector.swift
//  Sabores
//
//  Created by victor salazar on 24/09/15.
//  Copyright © 2015 Victor Salazar. All rights reserved.
//
import Foundation
class ServiceConnector{
    class func connectToUrl(url:String, params:AnyObject? = nil, response:@escaping ((AnyObject?, Error?) -> Void)){
        let req = NSMutableURLRequest(url: URL(string: url)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 30)
        req.addValue("application/json", forHTTPHeaderField:"Content-Type")
        req.httpMethod = "POST"
        if params != nil {
            if let dicParams = params as? NSDictionary {
                do{
                    req.httpBody = try JSONSerialization.data(withJSONObject: dicParams, options: JSONSerialization.WritingOptions())
                } catch let error as NSError {
                    response(nil, error)
                    return
                }
            }else if let strParams = params as? String {
                req.httpBody = strParams.data(using: String.Encoding.utf8)
            }
        }
        let task = URLSession.shared.dataTask(with: req as URLRequest, completionHandler: {data, res, error -> Void in
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
            }
            var finalError:Error? = error
            if finalError == nil {
                var result:AnyObject? = nil
                do{
                    result = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as AnyObject
                } catch let jsonError as NSError {
                    let stringTemp = String(data: data!, encoding: String.Encoding.utf8)
                    print(stringTemp as Any)
                    result = nil
                    finalError = jsonError
                }
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    response(result, finalError)
                }
            }else{
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    response(nil, error)
                }
            }
        })
        task.resume()
    }
}
