//
//  ProyectoLista1TableViewController.swift
//  Camposol
//
//  Created by victor salazar on 7/09/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
import UIKit
class ProjectsTableViewController:UITableViewController{
    //MARK: - Variables
    var arrProjects:[[String:AnyObject]] = []
    var approvalOption:Int = 0
    var rowToDelete:Int?
    var shouldDelete = false
    //MARK: - ViewController
    override func viewDidLoad(){
        super.viewDidLoad()
        self.navigationItem.title = ["Proyectos", "Recursos"][approvalOption]
    }
    override func viewDidAppear(_ animated:Bool){
        super.viewWillAppear(animated)
        if shouldDelete {
            shouldDelete = false
            self.arrProjects.remove(at: rowToDelete!)
            self.tableView.beginUpdates()
            self.tableView.deleteRows(at: [IndexPath(row: rowToDelete!, section: 0)], with: .top)
            self.tableView.endUpdates()
            rowToDelete = nil
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let button = sender as? UIButton {
            let viewCont = segue.destination as! ObservationViewController
            viewCont.dicProj = arrProjects[button.tag]
            viewCont.approvalOption = approvalOption
            rowToDelete = button.tag
        }
    }
    //MARK: - UITableView
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    override func tableView(_ tableView:UITableView, heightForFooterInSection section:Int) -> CGFloat {
        return 0.1
    }
    override func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return arrProjects.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "projectCell", for: indexPath as IndexPath) as! ProjectTableViewCell
        cell.fullLengthView.transform = CGAffineTransform.identity
        let dicProject = arrProjects[indexPath.row]
        let key = ["no_proyecto", "no_integrante"][approvalOption]
        cell.projectNameLbl.text = dicProject[key] as? String
        cell.budgetedAmountLbl.text = "S/. \(dicProject["mt_presupuesto_estimado"] as! Int)"
        cell.timeLbl.text = "\(dicProject["duration"] as! Int)"
        cell.numberPhasesLbl.text = "\(dicProject["nro_fases"] as! Int)"
        cell.startDateLbl.text = dicProject["fe_inicio"] as? String
        cell.endDateLbl.text = dicProject["fe_fin"] as? String
        cell.projStateLbl.text = dicProject["no_estado"] as? String
        cell.approvaleBtn.tag = indexPath.row
        cell.observeBtn.tag = indexPath.row
        cell.disapproveBtn.tag = indexPath.row
        print("*\(cell.projStateLbl.text!)*")
        cell.fullLengthView.gestureRecognizers!.first!.isEnabled = cell.projStateLbl.text! == "Por Aprobar"
        cell.fullLengthView.backgroundColor = cell.projStateLbl.text! == "Por Aprobar" ? UIColor.white : UIColor.lightGray
        cell.rightView.backgroundColor = cell.projStateLbl.text! == "Por Aprobar" ? UIColor(red: 19/255, green: 87/155, blue: 5/255, alpha: 1) : UIColor.clear
        return cell
    }
    //MARK: - IBAction
    @IBAction func aprobarFormulario(sender:UIButton){
        let alertCont = Toolbox.createAlertCont(title: "Aprobar", withMessage: "¿Desea aprobar el proyecto?")
        alertCont.addAction(UIAlertAction(title: "Si", style: .default, handler: { (alertCont) -> Void in
            //aprobar
            let dicProjTemp = self.arrProjects[sender.tag]
            let projId = dicProjTemp["id_proyecto"] as! Int
            let userId = Manager.defaultManager.userId
            var dicParams:Dictionary<String, AnyObject> = ["id_proyecto": projId as AnyObject, "id_usuario": userId as AnyObject, "nu_tipoAp": "1" as AnyObject, "no_proyecto": "" as AnyObject]
            if self.approvalOption == 1 {
                dicParams["id_integrante"] = dicProjTemp["id_integrante"] as! Int as AnyObject
            }
            print(dicParams)
            SVProgressHUD.show(withStatus: "Cargando")
            let url = self.approvalOption == 0 ? URLs.urlUpdateProject : URLs.urlUpdateResource
            AppDelegate.sharedAppDelegate().resetIdleTimer()
            ServiceConnector.connectToUrl(url: url, params: dicParams as AnyObject, response:{(result:AnyObject?, error:Error?) -> Void in
                if error == nil {
                    print("\(String(describing: result))")
                    if let dicResult = result as? Dictionary<String, AnyObject> {
                        let alertCont2 = Toolbox.createAlertCont(title: "Alerta", withMessage: dicResult["msg_retorno"] as! String)
                        alertCont2.addAction(UIAlertAction(title: "OK", style: .default, handler:{(action:UIAlertAction) -> Void in
                            let tipo = Int(dicResult["nro_retorno"] as! Int)
                            if tipo > 0 {
                                self.arrProjects.remove(at: sender.tag)
                                self.tableView.beginUpdates()
                                self.tableView.deleteRows(at: [IndexPath(row: sender.tag, section: 0)], with: .top)
                                self.tableView.endUpdates()
                            }
                        }))
                        self.present(alertCont2, animated: true, completion: nil)
                    }
                }else{
                    print("formState error: \(String(describing: error))")
                }
            })
        }))
        alertCont.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        present(alertCont, animated: true, completion: nil)
    }
    @IBAction func rechazarFormulario(sender:UIButton){
        let alertCont = Toolbox.createAlertCont(title: "Rechazar", withMessage: "¿Desea rechazar el proyectos?")
        alertCont.addAction(UIAlertAction(title: "Si", style: .default, handler: { (alertCont) -> Void in
            let dicProjTemp = self.arrProjects[sender.tag]
            let projId = dicProjTemp["id_proyecto"] as! Int
            let userId = Manager.defaultManager.userId
            var dicParams:Dictionary<String, AnyObject> = ["id_proyecto": projId as AnyObject, "id_usuario": userId as AnyObject, "nu_tipoAp": "2" as AnyObject, "no_proyecto": "" as AnyObject]
            if self.approvalOption == 1 {
                dicParams["id_integrante"] = dicProjTemp["id_integrante"] as AnyObject
            }
            SVProgressHUD.show(withStatus: "Cargando")
            let url = self.approvalOption == 0 ? URLs.urlUpdateProject : URLs.urlUpdateResource
            AppDelegate.sharedAppDelegate().resetIdleTimer()
            ServiceConnector.connectToUrl(url: url, params: dicParams as AnyObject, response:{(result:AnyObject?, error:Error?) -> Void in
                if error == nil {
                    print(result as Any)
                    if let dicResult = result as? Dictionary<String, String> {
                        let alertCont2 = Toolbox.createAlertCont(title: "Alerta", withMessage: dicResult["msg_retorno"]!)
                        alertCont2.addAction(UIAlertAction(title: "OK", style: .default, handler:{(action:UIAlertAction) -> Void in
                            let tipo = Int(dicResult["nro_retorno"]!)!
                            if tipo > 0 {
                                self.arrProjects.remove(at: sender.tag)
                                self.tableView.beginUpdates()
                                self.tableView.deleteRows(at: [IndexPath(row: sender.tag, section: 0)], with: .top)
                                self.tableView.endUpdates()
                            }
                        }))
                        self.present(alertCont2, animated: true, completion: nil)
                    }
                }else{
                    print("formState error: \(String(describing: error))")
                }
            })
        }))
        alertCont.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: nil))
        present(alertCont, animated: true, completion: nil)
    }
    @IBAction func observarFormulario(sender:UIButton){
        let alertCont = Toolbox.createAlertCont(title: "Observar", withMessage: "¿Desearia registrar una observación?")
        alertCont.addAction(UIAlertAction(title: "Si", style: .default, handler: { (alertCont) -> Void in
            self.performSegue(withIdentifier: "showObservation", sender: sender)
        }))
        alertCont.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        present(alertCont, animated: true, completion: nil)
    }
}
