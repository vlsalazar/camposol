//
//  ViewController.swift
//  Camposol
//
//  Created by Victor Salazar on 24/08/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
import UIKit
let usernameKey = "com.dsb.camposol.username"
let passwordKey = "com.dsb.camposol.password"
class LoginViewController:UIViewController{
    //MARK: - Variables
    var alertCont:UIAlertController?
    //MARK: - IBOutlet
    @IBOutlet weak var loginView:UIView!
    @IBOutlet weak var loginSubView:UIView!
    @IBOutlet weak var userTxtFld:UITextField!
    @IBOutlet weak var passwordTxtFld:UITextField!
    @IBOutlet weak var recordUserSwitch:UISwitch!
    //MARK: - ViewController
    override func viewDidLoad(){
        super.viewDidLoad()
        loginSubView.layer.borderColor = UIColor(red: 13/255.0, green: 95/255.0, blue: 40/255.0, alpha: 1.0).cgColor
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        if let username = UserDefaults.standard.string(forKey: usernameKey) {
            self.userTxtFld.text = username
            self.passwordTxtFld.text = UserDefaults.standard.string(forKey: passwordKey)
            self.recordUserSwitch.isOn = true
        }
    }
    //MARK: - IBAction
    @IBAction func dismissKeyboard(){
        userTxtFld.resignFirstResponder()
        passwordTxtFld.resignFirstResponder()
        loginView.transform = CGAffineTransform.identity
    }
    @IBAction func login(){
        userTxtFld.resignFirstResponder()
        passwordTxtFld.resignFirstResponder()
        if UIApplication.shared.isRegisteredForRemoteNotifications {
            if Manager.defaultManager.deviceId.length == 0 {
                UIApplication.shared.registerForRemoteNotifications()
                NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.managerHasDeviceId), name: Notification.Name.init("managerDeviceId"), object: nil)
            }else{
                print("deviceId: \(Manager.defaultManager.deviceId)")
                logging()
            }
        }else{
            let userSettings = UIUserNotificationSettings(types: [.alert, .sound, .badge], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(userSettings)
            UIApplication.shared.registerForRemoteNotifications()
            NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.managerHasDeviceId), name: Notification.Name.init("managerDeviceId"), object: nil)
        }
    }
    @IBAction func showDeviceId(){
        Toolbox.showAlertWithTitle(title: "DeviceId", withMessage: Manager.defaultManager.deviceId, inViewCont: self)
    }
    @IBAction func changeBaseURL(){
        if alertCont == nil {
            alertCont = UIAlertController(title: "URL Base", message: nil, preferredStyle: .alert)
            alertCont!.addTextField{(baseUrlTxtFld:UITextField) in
                baseUrlTxtFld.placeholder = "Ingrese el url aqui."
                baseUrlTxtFld.addTarget(self, action: #selector(LoginViewController.baseURLTxtFldDidEditing(baseUrlTxtFld:)), for: .editingChanged)
            }
            alertCont!.addAction(UIAlertAction(title: "Cancelar", style: .default, handler: nil))
            alertCont!.addAction(UIAlertAction(title: "Guardar", style: .default){(saveAction:UIAlertAction) in
                let baseUrlTxtFld = self.alertCont!.textFields!.first!
                UserDefaults.standard.set(baseUrlTxtFld.text!, forKey: baseUrlKey)
                UserDefaults.standard.synchronize()
            })
        }
        self.alertCont!.textFields!.first!.text = UserDefaults.standard.object(forKey: baseUrlKey) as? String
        self.alertCont!.actions[1].isEnabled = true
        self.present(alertCont!, animated: true, completion: nil)
    }
    //MARK: - Keyboard
    @objc func keyboardWillShow(notification:NSNotification){
        if self.loginView.transform.isIdentity {
            var info = notification.userInfo!
            let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
            let keyboardHeight = keyboardSize.height
            let bottomSpace = view.frame.size.height - loginView.frame.maxY - 10
            let translateY = bottomSpace < keyboardHeight ? bottomSpace - keyboardHeight : 0
            let animationDuration = (info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
            UIView.animate(withDuration: animationDuration, delay: 0.0, options: .curveEaseInOut, animations: {
                self.loginView.transform = CGAffineTransform(translationX: 0, y: translateY)
                }, completion: nil)
        }
    }
    @objc func keyboardWillHide(notification:NSNotification){
        var info = notification.userInfo!
        let animationDuration = (info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        UIView.animate(withDuration: animationDuration, delay: 0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            self.loginView.transform = CGAffineTransform.identity
        }, completion: nil)
    }
    //MARK: - Auxiliar
    func logging(){
        let dicParams = ["no_login": self.userTxtFld.text!, "no_password": self.passwordTxtFld.text!, "id_dispositivo": Manager.defaultManager.deviceId, "tip_disp": "I"]
        SVProgressHUD.show(withStatus: "Iniciando Sesión")
        ServiceConnector.connectToUrl(url: URLs.urlLogin, params: dicParams as AnyObject){(result:AnyObject?, error:Error?) -> Void in
            SVProgressHUD.dismiss()
            if error == nil {
                if let dicResult = result as? Dictionary<String, AnyObject> {
                    let userId = dicResult["id"] as! Int
                    if userId == 0 {
                        let message = dicResult["msg_error"] as! String
                        Toolbox.showAlertWithTitle(title: "Alerta", withMessage: message, inViewCont: self)
                    }else{
                        if self.recordUserSwitch.isOn {
                            UserDefaults.standard.set(self.userTxtFld.text, forKey: usernameKey)
                            UserDefaults.standard.set(self.passwordTxtFld.text, forKey: passwordKey)
                            UserDefaults.standard.synchronize()
                        }else{
                            self.userTxtFld.text = ""
                            self.passwordTxtFld.text = ""
                        }
                        Manager.defaultManager.userId = userId
                        Manager.defaultManager.timeInactivity = dicResult["tim_inac"] as! Int
                        self.performSegue(withIdentifier: "showMenu", sender: self)
                    }
                }else{
                    Toolbox.showErrorConnectionInViewCont(viewCont: self)
                }
            }else{
                Toolbox.showErrorConnectionInViewCont(viewCont: self)
            }
        }
    }
    @objc func managerHasDeviceId(){
        logging()
    }
    @objc func baseURLTxtFldDidEditing(baseUrlTxtFld:UITextField){
        self.alertCont!.actions[1].isEnabled = baseUrlTxtFld.text!.count > 0
    }
}
