//
//  ObservacionViewController.swift
//  Camposol
//
//  Created by Victor Salazar on 27/08/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
import UIKit
class ObservationViewController:UIViewController{
    @IBOutlet weak var scrollView:UIScrollView!
    @IBOutlet weak var observacionTxtView:UITextView!
    @IBOutlet weak var enviarBtn:UIButton!
    @IBOutlet weak var formNameLbl:UILabel!
    @IBOutlet weak var formDateLbl:UILabel!
    var dicForm:Dictionary<String, AnyObject>?
    var dicProj:Dictionary<String, AnyObject>?
    var approvalOption = 0
    override func viewDidLoad(){
        super.viewDidLoad()
        observacionTxtView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        observacionTxtView.layer.borderColor = UIColor(red: 0.66, green: 0.66, blue: 0.66, alpha: 0.66).cgColor
        enviarBtn.layer.borderColor = UIColor(red: 0.66, green: 0.66, blue: 0.66, alpha: 0.66).cgColor
        NotificationCenter.default.addObserver(self, selector: #selector(ObservationViewController.keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ObservationViewController.keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        if dicForm != nil {
            self.formNameLbl.text = dicForm!["nom"] as? String
            self.formDateLbl.text = dicForm!["fec"] as? String
        }else{
            self.formNameLbl.text = (self.approvalOption == 0 ? dicProj!["no_proyecto"] : dicProj!["no_integrante"]) as? String
            self.formDateLbl.text = "\(dicProj!["fe_inicio"] as! String) - \(dicProj!["fe_fin"] as! String)"
        }
    }
    override func viewDidDisappear(_ animated:Bool){
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    @IBAction func dismissKeyboard(_: AnyObject){
        observacionTxtView.resignFirstResponder()
    }
    @IBAction func sendObservation(_: AnyObject){
        if observacionTxtView.text.isEmpty {
            let alertCont = Toolbox.createAlertCont(title: "Alerta", withMessage: "El campo de texto no debe estar vacio.")
            alertCont.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alertCont, animated: true, completion: nil)
        }else{
            var url = ""
            var dicParams:Dictionary<String, AnyObject> = ["a": "" as AnyObject]
            if dicForm != nil {
                url = URLs.urlFormState
                let formId = dicForm!["idf"] as! Int
                let userId = Manager.defaultManager.userId
                dicParams = ["idf": formId, "idu": userId, "opc": "O", "obs": self.observacionTxtView.text] as Dictionary<String, AnyObject>
            }else{
                url = self.approvalOption == 0 ? URLs.urlUpdateProject : URLs.urlUpdateResource
                let projId = dicProj!["id_proyecto"] as! Int
                let userId = Manager.defaultManager.userId
                dicParams = ["id_proyecto": projId, "id_usuario": userId, "nu_tipoAp": "1", "no_proyecto": ""] as Dictionary<String, AnyObject>
                if self.approvalOption == 1 {
                    dicParams["id_integrante"] = dicProj!["id_integrante"] as AnyObject
                }
            }
            SVProgressHUD.show(withStatus: "Cargando")
            AppDelegate.sharedAppDelegate().resetIdleTimer()
            ServiceConnector.connectToUrl(url: url, params: dicParams as AnyObject, response:{(result:AnyObject?, error:Error?) -> Void in
                if error == nil {
                    if let dicResult = result as? Dictionary<String, String> {
                        let alertCont2 = Toolbox.createAlertCont(title: "Alerta", withMessage: dicResult["msg"]!)
                        alertCont2.addAction(UIAlertAction(title: "OK", style: .default, handler:{(action:UIAlertAction) -> Void in
                            if let tipoTemp = dicResult["tipo"] {
                                let tipo = Int(tipoTemp)
                                if tipo == 1 {
                                    self.navigationController!.popViewController(animated: true)
                                    let viewCont = self.navigationController!.viewControllers[1] as! FormulariosTableViewController
                                    viewCont.shouldDelete = true
                                }
                            }
                        }))
                        self.present(alertCont2, animated: true, completion: nil)
                    }else{
                        Toolbox.showErrorConnectionInViewCont(viewCont: self)
                    }
                }else{
                    Toolbox.showErrorConnectionInViewCont(viewCont: self)
                }
            })
        }
    }
    @objc func keyboardWillShow(notification:NSNotification){
        var info = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let keyboardHeight = keyboardSize.height
        scrollView.setContentOffset(CGPoint(x: 0, y: 89), animated: true)
        let contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardHeight, right: 0)
        scrollView.contentInset = contentInset
        scrollView.scrollIndicatorInsets = contentInset
    }
    @objc func keyboardWillHide(notification:NSNotification){
        let contentInset = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
        scrollView.scrollIndicatorInsets = contentInset
    }
}
