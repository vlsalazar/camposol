//
//  Extensions.swift
//  Camposol
//
//  Created by Victor Salazar on 7/09/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
import Foundation
extension String{
    var length:Int {return self.count}
    subscript(r:Range<Int>) -> String {
        get {
            let startIndex = self.index(self.startIndex, offsetBy: r.lowerBound)
            let endIndex = self.index(self.startIndex, offsetBy: r.upperBound)
            return String(self[startIndex ..< endIndex])
        }
    }
    func substring(from fr:Int) -> String {
        let end = self.count
        return self[fr ..< end]
    }
    func substring(from fr:Int, length l:Int) -> String {
        let end = fr + l
        return self[fr ..< end]
    }
    func substring(to t:Int) -> String {
        return self[0 ..< t]
    }
}
