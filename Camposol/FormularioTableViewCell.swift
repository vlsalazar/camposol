//
//  FormularioTableViewCell.swift
//  Camposol
//
//  Created by Victor Salazar on 26/08/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
import UIKit
class FormularioTableViewCell:PannableTableViewCell{
    @IBOutlet weak var nombreFormularioLbl:UILabel!
    @IBOutlet weak var recursoLbl:UILabel!
    @IBOutlet weak var fechaLbl:UILabel!
    @IBOutlet weak var aprobarBtn:UIButton!
    @IBOutlet weak var noPermitirBtn:UIButton!
    @IBOutlet weak var desaprobarBtn:UIButton!
    @IBOutlet weak var observarBtn:UIButton!
}