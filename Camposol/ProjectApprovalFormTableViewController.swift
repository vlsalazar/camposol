//
//  AprobacionProyectoTableViewController.swift
//  Camposol
//
//  Created by Victor Salazar on 2/09/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
import UIKit
class ProjectApprovalFormTableViewController:UITableViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate{
    let textFieldBorderColor = UIColor(red: 17/255.0, green: 139/255.0, blue: 186/255.0, alpha: 1.0).cgColor
    var currentTxtFld:UITextField?
    var arrResult:[[String : AnyObject]]?
    let arrOptions = ["Proyecto",  "Recurso"]
    var selectedOption = 0
    override func viewDidLoad(){
        super.viewDidLoad()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 50
    }
    override func viewWillAppear(_ animated:Bool){
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(ProjectApprovalFormTableViewController.keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ProjectApprovalFormTableViewController.keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        currentTxtFld?.resignFirstResponder()
        NotificationCenter.default.removeObserver(self)
    }
    //MARK: - Segue
    override func prepare(for segue:UIStoryboardSegue, sender: Any?) {
        let viewCont = segue.destination as! ProjectsTableViewController
        viewCont.arrProjects = arrResult!
        viewCont.approvalOption = selectedOption
    }
    //MARK: - UITableView
    override func tableView(_ tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        return 0.1
    }
    override func tableView(_ tableView:UITableView, heightForFooterInSection section:Int) -> CGFloat {
        return 0.1
    }
    override func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return 4
    }
    override func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "datesFilterCell", for: indexPath) as! ProjApprFromFilterDatesTableViewCell
            cell.startDateTxtFld.layer.borderColor = textFieldBorderColor
            cell.startDateTxtFld.inputView = createDatePicker(tag: 0)
            cell.startDateTxtFld.text = Toolbox.dateToString(date: Date(), withFormat: "dd/MM/yy")
            cell.endDateTxtFld.layer.borderColor = textFieldBorderColor
            cell.endDateTxtFld.inputView = createDatePicker(tag: 1)
            cell.endDateTxtFld.text = Toolbox.dateToString(date: Date(), withFormat: "dd/MM/yy")
            return cell
        }else if indexPath.row == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "buscarBtnCell", for: indexPath)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "valueFilterCell", for: indexPath) as! ProjApprFromFilterTxtFldTableViewCell
            cell.valueTxtFld.inputView = nil
            cell.valueTxtFld.layer.borderColor = textFieldBorderColor
            if indexPath.row == 0 {
                let pickerView = UIPickerView()
                pickerView.dataSource = self
                pickerView.delegate = self
                cell.valueTxtFld.inputView = pickerView
                cell.nameLbl.text = "Opción de Aprobación"
                cell.valueTxtFld.text = arrOptions[selectedOption]
            }else{
                cell.nameLbl.text = ["Nombre de Proyecto", "Nombre de Persona"][selectedOption]
                cell.valueTxtFld.text = ""
            }
            return cell
        }
    }
    //MARK: - UIPickerView
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView:UIPickerView, numberOfRowsInComponent component:Int) -> Int {
        return arrOptions.count
    }
    func pickerView(_ pickerView:UIPickerView, titleForRow row:Int, forComponent component:Int) -> String? {
        return arrOptions[row]
    }
    func pickerView(_ pickerView:UIPickerView, didSelectRow row:Int, inComponent component:Int){
        if row != selectedOption {
            selectedOption = row
            currentTxtFld?.text = arrOptions[row]
            self.tableView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .none)
        }
    }
    //MARK: - UITextField
    func textFieldDidBeginEditing(_ textField:UITextField){
        currentTxtFld = textField
    }
    //MARK: - IBAction
    @IBAction func dismissKeyboard(_: AnyObject){
        currentTxtFld?.resignFirstResponder()
    }
    @IBAction func search(){
        currentTxtFld?.resignFirstResponder()
        let txtFldCell = tableView.cellForRow(at: IndexPath(row: 1, section: 0)) as! ProjApprFromFilterTxtFldTableViewCell
        let datesCell = tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as! ProjApprFromFilterDatesTableViewCell
        let filterText = txtFldCell.valueTxtFld.text!
        let texts = [filterText, ""]
        let startDateDatePicker = datesCell.startDateTxtFld.inputView as! UIDatePicker
        let endDateDatePicker = datesCell.endDateTxtFld.inputView as! UIDatePicker
        let strStartDate = Toolbox.dateToString(date: startDateDatePicker.date, withFormat: "dd/MM/yyyy")
        let strEndDate = Toolbox.dateToString(date: endDateDatePicker.date, withFormat: "dd/MM/yyyy")
        let dicParams = ["nu_tipoAp": (selectedOption + 1), "id_usuario": Manager.defaultManager.userId, "no_responsable": texts[1 - selectedOption], "no_proyecto": texts[selectedOption], "fe_inicio": strStartDate, "fe_fin": strEndDate] as [String : Any]
        SVProgressHUD.show(withStatus: "Cargando")
        AppDelegate.sharedAppDelegate().resetIdleTimer()
        ServiceConnector.connectToUrl(url: URLs.urlListProjects, params: dicParams as AnyObject){(result:AnyObject?, error:Error?) in
            if error == nil {
                self.arrResult = result as? Array<Dictionary<String, AnyObject>>
                if self.arrResult!.count == 0 {
                    Toolbox.showAlertWithTitle(title: "Alerta", withMessage: "No se encontro ningún proyecto.", inViewCont: self)
                }else{
                    self.performSegue(withIdentifier: "showProjects", sender:nil)
                }
            }else{
                print(error as Any)
            }
        }
    }
    //MARK: - Keyboard
    @objc func keyboardWillShow(notification:NSNotification){
        var info = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        if self.tableView.frame.size.height - self.tableView.contentSize.height < keyboardSize.height {
            let contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height, right: 0)
            tableView.contentInset = contentInset
            tableView.scrollIndicatorInsets = contentInset
        }
    }
    @objc func keyboardWillHide(notification:NSNotification){
        let contentInset = UIEdgeInsets.zero
        tableView.contentInset = contentInset
        tableView.scrollIndicatorInsets = contentInset
    }
    //MARK: - UIDatePicker
    func createDatePicker(tag:Int) -> UIDatePicker {
        let datePicker = UIDatePicker()
        datePicker.locale = Locale(identifier: "es_PE")
        datePicker.datePickerMode = UIDatePickerMode.date
        datePicker.tag = tag
        datePicker.addTarget(self, action: #selector(ProjectApprovalFormTableViewController.datePickerChangeValue(datePicker:)), for: .valueChanged)
        return datePicker
    }
    @objc func datePickerChangeValue(datePicker:UIDatePicker){
        currentTxtFld?.text = Toolbox.dateToString(date: datePicker.date, withFormat: "dd/MM/yy")
        let datesCell = tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as! ProjApprFromFilterDatesTableViewCell
        if datePicker.tag == 0 {
            let endDateTxtFld = datesCell.endDateTxtFld
            let endDateDatePicker = endDateTxtFld?.inputView as! UIDatePicker
            if datePicker.date.compare(endDateDatePicker.date) == ComparisonResult.orderedDescending {
                endDateDatePicker.date = datePicker.date
                endDateTxtFld?.text = currentTxtFld!.text
            }
        }else{
            let startDateTxtFld = datesCell.startDateTxtFld
            let startDateDatePicker = startDateTxtFld?.inputView as! UIDatePicker
            if datePicker.date.compare(startDateDatePicker.date) == ComparisonResult.orderedAscending {
                startDateDatePicker.date = datePicker.date
                startDateTxtFld?.text = currentTxtFld!.text
            }
        }
    }
}
