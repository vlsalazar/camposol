//
//  AprobProyTipoFiltro2TableViewCell.swift
//  Camposol
//
//  Created by Victor Salazar on 3/09/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
import UIKit
class ProjApprFromFilterDatesTableViewCell:UITableViewCell{
    @IBOutlet weak var startDateTxtFld:UITextField!
    @IBOutlet weak var endDateTxtFld:UITextField!
}