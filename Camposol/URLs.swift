//
//  URLs.swift
//  Camposol
//
//  Created by Victor Salazar on 4/09/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
import Foundation
class URLs{
    static let baseUrl = UserDefaults.standard.object(forKey: baseUrlKey) as! String
    static let urlRoot = "\(baseUrl)/EASYWEB_WS/"
    static let urlProyectsRoot = "\(urlRoot)Proyecto/wsProyecto.svc/"
    static let urlFormsRoot = "\(urlRoot)wsFormulario.svc/"
    static let urlLogin = "\(urlFormsRoot)LoginUserMobileActiveDirectory"
    static let urlListFormularios = "\(urlFormsRoot)LIS_Formularios"
    static let urlLogout = "\(urlFormsRoot)UDP_Cerrar_Sesion"
    static let urlFormState = "\(urlFormsRoot)UDP_Estado_Formulario"
    static let urlListProjects = "\(urlProyectsRoot)LIS_Proyectos"
    static let urlUpdateProject = "\(urlProyectsRoot)ARO_Proyecto"
    static let urlUpdateResource = "\(urlProyectsRoot)ARO_Integrante"
}
