//
//  OptionTableViewCell.swift
//  Camposol
//
//  Created by Victor Salazar on 25/08/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
import UIKit
class OptionTableViewCell:UITableViewCell{
    @IBOutlet weak var iconImgView:UIImageView!
    @IBOutlet weak var optionLabel:UILabel!
    @IBOutlet weak var arrowImgView:UIImageView!
}