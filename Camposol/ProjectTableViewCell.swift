//
//  ProyectoItem1TableViewCell.swift
//  Camposol
//
//  Created by victor salazar on 7/09/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
import UIKit
class ProjectTableViewCell:PannableTableViewCell{
    @IBOutlet weak var projectNameLbl:UILabel!
    @IBOutlet weak var budgetedAmountLbl:UILabel!
    @IBOutlet weak var timeLbl:UILabel!
    @IBOutlet weak var numberPhasesLbl:UILabel!
    @IBOutlet weak var startDateLbl:UILabel!
    @IBOutlet weak var endDateLbl:UILabel!
    @IBOutlet weak var projStateLbl:UILabel!
    @IBOutlet weak var rightView:UIView!
    @IBOutlet weak var approvaleBtn:UIButton!
    @IBOutlet weak var observeBtn:UIButton!
    @IBOutlet weak var disapproveBtn:UIButton!
}