//
//  Toolbox.swift
//  Camposol
//
//  Created by Victor Salazar on 27/08/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
import Foundation
import UIKit
class Toolbox{
    class func createAlertCont(title:String?, withMessage message:String) -> UIAlertController{
        return UIAlertController(title: title, message: message, preferredStyle:.alert)
    }
    class func transformDictionaryToData(dic:Dictionary<String, AnyObject>) -> Data? {
        var error:NSError? = nil
        var data:Data?
        do {
            data = try JSONSerialization.data(withJSONObject: dic, options: JSONSerialization.WritingOptions())
        } catch let error1 as NSError {
            error = error1
            data = nil
        }
        if error != nil {
            return nil
        }else{
            return data
        }
    }
    class func createRequestWithURL(url:NSURL, withBody bodyData:Data?) -> NSURLRequest {
        let urlRequest = NSMutableURLRequest(url: url as URL)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = bodyData
        return urlRequest
    }
    class func cleanJSONData(data:Data) -> Data {
        var strTemp = NSString(data: data, encoding:String.Encoding.utf8.rawValue)! as String
        if strTemp.first == "\"" {
            strTemp = strTemp.substring(from: 1)
        }
        if strTemp.substring(from: strTemp.length - 1) == "\"" {
            strTemp = strTemp.substring(to: strTemp.length - 1)
        }
        strTemp = strTemp.replacingOccurrences(of: "\\", with: "")
        return strTemp.data(using: String.Encoding.utf8)!
    }
    class func isErrorConnectionInternet(errorCode:Int) -> Bool {
        if errorCode == NSURLErrorCannotFindHost || errorCode == NSURLErrorCannotConnectToHost || errorCode == NSURLErrorNetworkConnectionLost || errorCode == NSURLErrorDNSLookupFailed || errorCode == NSURLErrorHTTPTooManyRedirects || errorCode == NSURLErrorNotConnectedToInternet || errorCode == NSURLErrorSecureConnectionFailed || errorCode == NSURLErrorCannotLoadFromNetwork {
            return true
        }else{
            return false
        }
    }
    class func createDateFormatterWithFormat(format:String) -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = format
        return dateFormatter
    }
    class func stringToDate(strDate:String, withFormat format:String) -> Date? {
        let dateFormatter = getDateFormatterWithFormat(format: format)
        return dateFormatter.date(from: strDate)
    }
    class func dateToString(date:Date, withFormat format:String) -> String {
        let dateFormatter = getDateFormatterWithFormat(format: format)
        return dateFormatter.string(from: date)
    }
    class func getDateFormatterWithFormat(format:String) -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        dateFormatter.locale = Locale(identifier: "es_PE")
        dateFormatter.dateFormat = format
        return dateFormatter
    }
    class func showAlertWithTitle(title:String, withMessage message:String, withOkHandler handler:((_ alertAction:UIAlertAction) -> Void)? = nil, inViewCont viewCont:UIViewController){
        let alertCont = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertCont.addAction(UIAlertAction(title: "OK", style: .default, handler: handler))
        viewCont.present(alertCont, animated: true, completion: nil)
    }
    class func showErrorConnectionInViewCont(viewCont:UIViewController){
        self.showAlertWithTitle(title: "Alerta", withMessage: "Hubo error en la conexión.", inViewCont: viewCont)
    }
}
