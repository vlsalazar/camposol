//
//  CancelableClosure.swift
//  Camposol
//
//  Created by Victor Salazar on 25/11/15.
//  Copyright © 2015 Victor Salazar. All rights reserved.
//
import Foundation
typealias dispatch_cancelable_closure = (_ cancel:Bool) -> Void

func delay(time:TimeInterval, closure:@escaping ()->Void) -> dispatch_cancelable_closure? {
    func dispatch_later(clsr:@escaping ()->Void) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime(uptimeNanoseconds: UInt64(time) * 1000 * NSEC_PER_SEC), execute: clsr)
    }
    var cancelableClosure:dispatch_cancelable_closure?
    let delayedClosure:dispatch_cancelable_closure = { cancel in
        if cancel == false {
            DispatchQueue.main.async {
                closure()
            }
        }
        cancelableClosure = nil
    }
    cancelableClosure = delayedClosure
    dispatch_later{
        if let delayedClosure = cancelableClosure {
            delayedClosure(false)
        }
    }
    return cancelableClosure;
}
func cancel_delay(closure:dispatch_cancelable_closure?){
    if closure != nil {
        closure!(true)
    }
}
