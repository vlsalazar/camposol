//
//  FormulariosTableViewController.swift
//  Camposol
//
//  Created by Victor Salazar on 26/08/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
import UIKit
class FormulariosTableViewController:UITableViewController{
    //MARK: - Variables
    var arrForms:Array<Dictionary<String, AnyObject>> = []
    var rowToDelete:Int?
    var shouldDelete = false
    //MARK: - IBOutlet
    @IBOutlet weak var refreshCtrl:UIRefreshControl!
    //MARK: - ViewController
    override func viewDidLoad(){
        super.viewDidLoad()
        self.tableView.layoutMargins = UIEdgeInsets.zero
        SVProgressHUD.show(withStatus: "Cargando")
        let userId = Manager.defaultManager.userId
        ServiceConnector.connectToUrl(url: URLs.urlListFormularios, params: ["idf": userId] as AnyObject){(result:AnyObject?, error:Error?) -> Void in
            if error == nil {
                self.arrForms = result as! Array<Dictionary<String, AnyObject>>
                self.tableView.reloadData()
            }else{
                Toolbox.showErrorConnectionInViewCont(viewCont: self)
            }
        }
    }
    override func viewDidAppear(_ animated:Bool){
        super.viewWillAppear(animated)
        if shouldDelete {
            shouldDelete = false
            self.arrForms.remove(at: rowToDelete!)
            self.tableView.beginUpdates()
            if self.arrForms.count > 0 {
                self.tableView.deleteRows(at: [IndexPath(row: rowToDelete!, section: 0) as IndexPath], with: .top)
            }else{
                self.tableView.reloadRows(at: [IndexPath(row: 0, section: 0) as IndexPath], with: .none)
            }
            self.tableView.endUpdates()
            rowToDelete = nil
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let button = sender as? UIButton {
            let viewCont = segue.destination as! ObservationViewController
            let idx = self.arrForms.map(){$0["idf"] as! Int}.index(of: button.tag)!
            viewCont.dicForm = self.arrForms[idx]
            rowToDelete = idx
        }
    }
    //MARK: - TableView
    override func tableView(_ tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        return 0.1
    }
    override func tableView(_ tableView:UITableView, heightForFooterInSection section:Int) -> CGFloat {
        return 0.1
    }
    override func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return arrForms.count > 0 ? arrForms.count : 1
    }
    override func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        if arrForms.count > 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "formularioCell", for: indexPath) as! FormularioTableViewCell
            cell.layoutMargins = UIEdgeInsets.zero
            let dicForm = arrForms[indexPath.row]
            cell.nombreFormularioLbl.text = dicForm["nom"] as? String
            cell.recursoLbl.text = dicForm["usu"] as? String
            //let formDate = Toolbox.stringToDate(strDate: (dicForm["fec"] as! String).lowercased(), withFormat: "dd/MM/yyyy hh:mm a")
            cell.fechaLbl.text = dicForm["fec"] as? String
            cell.aprobarBtn.tag = dicForm["idf"] as! Int
            cell.noPermitirBtn.tag = dicForm["idf"] as! Int
            cell.desaprobarBtn.tag = dicForm["idf"] as! Int
            cell.observarBtn.tag = dicForm["idf"] as! Int
            cell.fullLengthView.transform = CGAffineTransform.identity
            return cell
        }else{
            return tableView.dequeueReusableCell(withIdentifier: "noFormsCell", for: indexPath)
        }
    }
    //MARK: - IBAction
    @IBAction func aprobarFormulario(_ sender:UIButton){
        let alertCont = Toolbox.createAlertCont(title: "Aprobar", withMessage: "¿Desea aprobar el formulario?")
        alertCont.addAction(UIAlertAction(title: "Si", style: .default, handler: { (alertCont) -> Void in
            let formId = sender.tag
            let userId = Manager.defaultManager.userId
            let dicParams = ["idf": formId, "idu": userId, "opc": "A", "obs": ""] as [String : Any]
            SVProgressHUD.show(withStatus: "Cargando")
            AppDelegate.sharedAppDelegate().resetIdleTimer()
            ServiceConnector.connectToUrl(url: URLs.urlFormState, params: dicParams as AnyObject, response:{(result:AnyObject?, error:Error?) -> Void in
                if error == nil {
                    if let dicResult = result as? Dictionary<String, String> {
                        let alertCont2 = Toolbox.createAlertCont(title: "Alerta", withMessage: dicResult["msg"]!)
                        alertCont2.addAction(UIAlertAction(title: "OK", style: .default, handler:{(action:UIAlertAction) -> Void in
                            if let tipoTemp = dicResult["tipo"] {
                                let tipo = Int(tipoTemp)
                                if tipo == 1 {
                                    let idx = self.arrForms.map(){$0["idf"] as! Int}.index(of: formId)!
                                    self.arrForms.remove(at: idx)
                                    self.tableView.beginUpdates()
                                    if self.arrForms.count > 0 {
                                        self.tableView.deleteRows(at: [IndexPath(row: idx, section: 0)], with: .top)
                                    }else{
                                        self.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
                                    }
                                    self.tableView.endUpdates()
                                }
                            }
                        }))
                        self.present(alertCont2, animated: true, completion: nil)
                    }else{
                        Toolbox.showErrorConnectionInViewCont(viewCont: self)
                    }
                }else{
                    Toolbox.showErrorConnectionInViewCont(viewCont: self)
                }
            })
        }))
        alertCont.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        present(alertCont, animated: true, completion: nil)
    }
    @IBAction func observarFormulario(_ sender:UIButton){
        let alertCont = Toolbox.createAlertCont(title: "Observar", withMessage: "¿Desearia registrar una observación?")
        alertCont.addAction(UIAlertAction(title: "Si", style: .default, handler: { (alertCont) -> Void in
            self.performSegue(withIdentifier: "showObservation", sender: sender)
        }))
        alertCont.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        present(alertCont, animated: true, completion: nil)
    }
    @IBAction func rechazarFormulario(_ sender:UIButton){
        let alertCont = Toolbox.createAlertCont(title: "Rechazar", withMessage: "¿Desea rechazar el formulario?")
        alertCont.addAction(UIAlertAction(title: "Si", style: .default, handler: { (alertCont) -> Void in
            let formId = sender.tag
            let userId = Manager.defaultManager.userId
            let dicParams = ["idf": formId, "idu": userId, "opc": "R", "obs": ""] as [String : Any]
            SVProgressHUD.show(withStatus: "Cargando")
            AppDelegate.sharedAppDelegate().resetIdleTimer()
            ServiceConnector.connectToUrl(url: URLs.urlFormState, params: dicParams as AnyObject, response:{(result:AnyObject?, error:Error?) -> Void in
                if error == nil {
                    if let dicResult = result as? Dictionary<String, String> {
                        let alertCont2 = Toolbox.createAlertCont(title: "Alerta", withMessage: dicResult["msg"]!)
                        alertCont2.addAction(UIAlertAction(title: "OK", style: .default, handler:{(action:UIAlertAction) -> Void in
                            if let tipoTemp = dicResult["tipo"] {
                                let tipo = Int(tipoTemp)
                                if tipo == 1 {
                                    let idx = self.arrForms.map(){return $0["idf"] as! Int}.index(of: formId)!
                                    self.arrForms.remove(at: idx)
                                    self.tableView.beginUpdates()
                                    if self.arrForms.count > 0 {
                                        self.tableView.deleteRows(at: [IndexPath(row: idx, section: 0)], with: .top)
                                    }else{
                                        self.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
                                    }
                                    self.tableView.endUpdates()
                                }
                            }
                        }))
                        self.present(alertCont2, animated: true, completion: nil)
                    }else{
                        Toolbox.showErrorConnectionInViewCont(viewCont: self)
                    }
                }else{
                    Toolbox.showErrorConnectionInViewCont(viewCont: self)
                }
            })
        }))
        alertCont.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        present(alertCont, animated: true, completion: nil)
    }
    @IBAction func verPDFdelFormulario(_ sender:UIButton){
        let formId = sender.tag
        UIApplication.shared.openURL(NSURL(string: "\(URLs.baseUrl)/EASYWEB/Formularios/CA_Formularios_Previsualizacion.aspx?f=\(formId)&t=F")! as URL)
    }
    @IBAction func searchForms(){
        let userId = Manager.defaultManager.userId
        ServiceConnector.connectToUrl(url: URLs.urlListFormularios, params: ["idf": userId] as AnyObject){(result:AnyObject?, error:Error?) -> Void in
            if error == nil {
                self.arrForms = result as! Array<Dictionary<String, AnyObject>>
                self.tableView.reloadData()
                self.refreshCtrl.endRefreshing()
            }else{
                Toolbox.showAlertWithTitle(title: "Alerta", withMessage: "Hubo error en la conexión.", inViewCont: self)
            }
        }
    }
}
